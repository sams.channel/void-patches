This is a patch for [ly - a TUI display manager](https://github.com/fairyglade/ly)
which fixes the F1 shutdown option on Void Linux systems
by substituting shutdown for poweroff.

Usage
----

Install `patch` first if necessary:
<pre># xbps-install -S patch</pre>


Navigate to cloned ly repository's src folder
and run the patch on config.c:

<pre>$ patch config.c < ly_f1_poweroff.patch</pre>

Build ly from source [following the instructions](https://github.com/fairyglade/ly/blob/master/readme.md).

